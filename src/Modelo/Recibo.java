//Rodriguez Sanchez Yennifer Carolina
//Examen Recuperacion
//23-06-2023
package Modelo;

public class Recibo {
    private int numRecibo;
    private String fecha;
    private String nombre;
    private String domicilo;
    private int tipoServicio;
    private float costoKilowatts;
    private float consumoKilowatts;
    
    //metodos constructores
    public Recibo(int numRecibo, float costoKilowatts, float consumoKilowatts){
        this.numRecibo = numRecibo;
        this.costoKilowatts = costoKilowatts;
        this.consumoKilowatts = costoKilowatts;
    }
    //metodo constructor copia
    public Recibo(Recibo R){
        this.numRecibo = R.numRecibo;
        this.costoKilowatts = R.costoKilowatts;
        this.consumoKilowatts = R.consumoKilowatts;
    }

    public String getdomicilio() {
        return domicilo;
    }

    public void setdomicilio(String domicilio) {
        this.domicilo = domicilio;
    }

    public int getnumRecibo() {
        return numRecibo;
    }

    public void setnumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int gettipoServicio() {
        return tipoServicio;
    }

    public void settipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public float getcostoKilowatts() {
        return costoKilowatts;
    }

    public void setcostoKilowatts(float costoKilowatts) {
        this.costoKilowatts = costoKilowatts;
    }

    public float getconsumoKilowatts() {
        return consumoKilowatts;
    }

    public void setconsumoKilowatts(float consumoKilowatts) {
        this.consumoKilowatts = consumoKilowatts;
    }
    
    public float SubTotal(){
        return this.consumoKilowatts*this.costoKilowatts;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    public float Impuesto() {
        return SubTotal() * 0.16f;
    }
    
    public float TotalAPagar(){
        return SubTotal()+Impuesto();
    }

}
