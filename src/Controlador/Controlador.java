//Rodriguez Sanchez Yennifer Carolina
//Examen Recuperacion
//23-06-2023
package Controlador;

import Vista.dlgVista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import Modelo.Recibo;

public class Controlador implements ActionListener{
    private Recibo R;
    private dlgVista vista;
    public Controlador(Recibo R,dlgVista vista){
        this.R =R;
        this.vista =vista;
        vista.btnCerrar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }
    private void iniciarVista(){
        vista.setTitle(":: Productos ::");
        vista.setSize(480,620);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==vista.btnNuevo){
            
            vista.txtNombre.setEnabled(true);
            vista.txtNumRecibo.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtConsumidoKilowatts.setEnabled(true);
            vista.jComboBox2.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
        }
        if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista,"Seguro que quieres salir",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
        if(e.getSource()==vista.btnGuardar){
            try{
                if(Integer.parseInt(vista.txtNumRecibo.getText()) <= 0){
                    throw new IllegalArgumentException("Ingrese un numero de recibo valido");
                }
                
                if(Float.parseFloat(vista.txtConsumidoKilowatts.getText()) <= 0){
                    throw new IllegalArgumentException("Ingrese un numero de Kilowatts valido");
                }
                R.setnumRecibo(Integer.parseInt(vista.txtNumRecibo.getText()));
                R.setNombre(vista.txtNombre.getText());
                R.setFecha(vista.txtFecha.getText());
                R.setdomicilio(vista.txtDomicilio.getText());
                R.setcostoKilowatts(Float.parseFloat(vista.txtCostoKilowatts.getText()));
                R.setconsumoKilowatts(Float.parseFloat(vista.txtConsumidoKilowatts.getText()));
                R.settipoServicio(vista.jComboBox2.getSelectedIndex());
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "ERROR: " + ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex2){
                JOptionPane.showMessageDialog(vista, "ERROR: " + ex2.getMessage());
                return;
            }
            

            vista.btnCancelar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
        }
        if(e.getSource()==vista.btnMostrar){
            vista.txtNombre.setText(R.getNombre());
            vista.txtFecha.setText(R.getFecha());
            vista.txtDomicilio.setText(R.getdomicilio());
            vista.txtNumRecibo.setText(Integer.toString(R.getnumRecibo()));
            vista.txtSubTotal.setText(Float.toString(R.SubTotal()));
            vista.txtImpuestos.setText(Float.toString(R.Impuesto()));
            vista.txtTotalAPagar.setText(Float.toString(R.TotalAPagar()));
            
        }
        
        if(e.getSource()==vista.btnCancelar){
            vista.txtDomicilio.setEnabled(false);
            vista.txtFecha.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtNumRecibo.setEnabled(false);
            vista.txtSubTotal.setEnabled(false);
            vista.txtTotalAPagar.setEnabled(false);
            vista.txtImpuestos.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
        }
        
        if(e.getSource()==vista.btnLimpiar){
            vista.txtDomicilio.setEnabled(false);
            vista.txtFecha.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtNumRecibo.setEnabled(false);
            vista.txtSubTotal.setEnabled(false);
            vista.txtTotalAPagar.setEnabled(false);
            vista.txtImpuestos.setEnabled(false);
        }
        }
        public static void main(String[] args) {
           Recibo R = null;
           dlgVista vista = new dlgVista(new JFrame(), true);
           Controlador contra = new Controlador(R, vista);
           contra.iniciarVista();
    }

}
